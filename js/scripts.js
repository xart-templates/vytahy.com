jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// popover
	$('.popover').each(function(){
		$('body').click(function(){
			$('.popover').fadeOut();
		});
		$('.inner',this).click(function(e){
			e.stopPropagation();
		});
		$(this).prepend('<i class="icon-cancel-circled close"/>');
		$('.close',this).click(function(){
			$(this).parents('.popover').fadeOut();
		});
	});
	$('[data-toggle="popover"]').click(function(e){
		e.stopPropagation();
		$(this).next('.popover').fadeToggle();
	});

	// configurator
	$('[data-toggle="configurator"]').click(function(){
		var item = $(this).attr('data-id');
		var name = $(this).attr('data-name');
		$(this).parents('.popover').find('li').removeClass('active');
		$(this).parents('li').addClass('active');
		$('[data-rel="configurator"] [data-id]').parents('.rendre').find('li').removeClass('active');
		$('[data-rel="configurator"] [data-id="'+item+'"]').parents('li').addClass('active');
		$('input[data-rel="configurator"]').val(name);
	});

	$('[data-toggle="component"]').change(function(){
		var item = $(this).attr('data-id');
		$('[data-rel="configurator"] [data-id="'+item+'"]').parents('li').toggleClass('active');
	});

	// ie6-9 condition 
	if (document.createElement('input').placeholder==undefined){
		// placeholder
		$('[placeholder]').focus(function(){var input=$(this);if(input.val()==input.attr('placeholder')){input.val('');input.removeClass('placeholder')}}).blur(function(){var input=$(this);if(input.val()==''||input.val()==input.attr('placeholder')){input.addClass('placeholder');input.val(input.attr('placeholder'))}}).blur()
	}

});